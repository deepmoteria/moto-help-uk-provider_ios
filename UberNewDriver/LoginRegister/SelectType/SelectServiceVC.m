//  SelectServiceVC.m
//  TaxiNow Driver
//  Created by My Mac on 3/23/15.
//  Copyright (c) 2015 Deep Gami. All rights reserved.

#import "SelectServiceVC.h"
#import "SelectServiceCell.h"
#import <CoreText/CoreText.h>
#import "SelectTowingServiceVC.h"

@interface SelectServiceVC ()
{
    NSMutableArray *arr;
    NSMutableArray *arrForTowingServiceType;
    NSMutableArray *arrType;
    NSMutableArray *arrPricet;
    BOOL isContain;
    NSMutableArray *arrWtype;
    NSMutableArray *arrDistancePrice,*arrTimePrice;
    NSInteger index;
    NSMutableString *strUserId;
    NSMutableString *strUserToken;
    CGRect serviceTableFrame;
}

@end

@implementation SelectServiceVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    arr=[[NSMutableArray alloc]init];
    arrForTowingServiceType=[[NSMutableArray alloc]init];
    arrType=[[NSMutableArray alloc]init];
    arrWtype=[[NSMutableArray alloc]init];
    arrDistancePrice=[[NSMutableArray alloc]init];
    arrTimePrice=[[NSMutableArray alloc]init];
    arrPricet=[[NSMutableArray alloc]init];
    NSLog(@"register details :%@",self.dictparam);
    NSLog(@"image  :%@",self.imgP);
    
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
    
    [self setLocalization];
    [self getType];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    self.tblselectService.contentInset = UIEdgeInsetsZero;
    self.automaticallyAdjustsScrollViewInsets = NO;
    serviceTableFrame = self.tblselectService.frame;
}

#pragma mark - custom methods

-(void)setLocalization
{
    [self.btnMenu setTitle:NSLocalizedString(@"SELECR_SERVICE", nil) forState:UIControlStateNormal];
    [self.btnMenu setTitle:NSLocalizedString(@"SELECR_SERVICE", nil) forState:UIControlStateHighlighted];
    self.btnMenu.titleLabel.font = [UberStyleGuide fontRegularLight];
}

-(void)getType
{
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getDataFromPath:FILE_WALKER_TYPE withParamData:nil withBlock:^(id response, NSError *error)
     {
         if (response)
         {
             response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
             
             if([[response valueForKey:@"success"] intValue]==1)
             {
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 
                 [arr addObjectsFromArray:[response valueForKey:@"types"]];
                 
                 [arrType removeAllObjects];
                 [arrPricet removeAllObjects];
                 [arrWtype removeAllObjects];
                 
                 for(int i=0;i<arr.count;i++)
                 {
                     if([[[arr objectAtIndex:i] valueForKey:@"is_type_flat"]isEqualToString:@"1"])
                     {
                         [arrType addObject:[arr objectAtIndex:i]];
                         [arrPricet addObject:@""];
                         [arrWtype addObject:@""];
                         [arrDistancePrice addObject:@"0"];
                         [arrTimePrice addObject:@"0"];
                     }
                     else
                     {
                         [arrForTowingServiceType addObject:[arr objectAtIndex:i]];
                     }
                 }
                 
                 NSLog(@"arr :%@",arrType);
                 
                 [self.tblselectService reloadData];
                 NSLog(@"Check %@",response);
             }
             else
             {
                 NSString *str = [response valueForKey:@"error_code"];
                 if([str intValue] == 406)
                 {
                     [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                 }
             }
         }
     }];
}

#pragma mark - textfield delagte method

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
    
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    return [string isEqualToString:filtered];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    UITextField *txtf=(UITextField *)textField;
    NSIndexPath *path = [NSIndexPath indexPathForRow:txtf.tag inSection:0];
    
    [textField resignFirstResponder];
    
    SelectServiceCell *cell=[self.tblselectService cellForRowAtIndexPath:path];
    
    cell.txtprice.text = textField.text;
    
    NSLog(@"index  :%ld",(long)txtf.tag);
    [arrPricet replaceObjectAtIndex:path.row withObject:cell.txtprice.text];
    NSLog(@"log :%@",arrPricet);
    
    self.tblselectService.frame = serviceTableFrame;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    self.tblselectService.frame = serviceTableFrame;
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint origin = textField.frame.origin;
    CGPoint point = [textField.superview convertPoint:origin toView:self.tblselectService];
    
    NSIndexPath *indexPath = [self.tblselectService indexPathForRowAtPoint:point];
    
    SelectServiceCell *cell=[self.tblselectService cellForRowAtIndexPath:indexPath];
    
    NSLog(@"indexPath = %ld",(long)indexPath.row);
    
    NSLog(@"Cell Frame = %f",cell.frame.origin.y+cell.frame.size.height);
    
    self.tblselectService.frame = serviceTableFrame;
    
    NSLog(@"View Frame Center = %f",self.tblselectService.frame.origin.y/2+self.tblselectService.frame.size.height/2);
    
    NSInteger difference = (cell.frame.origin.y+cell.frame.size.height - (self.tblselectService.frame.origin.y/2+self.tblselectService.frame.size.height/2));
    
    NSLog(@"Difference = %ld",(long)difference);
    
    if(self.tblselectService.frame.origin.y/2+self.tblselectService.frame.size.height/2 < cell.frame.origin.y+cell.frame.size.height)
    {
        NSInteger difference = (cell.frame.origin.y+cell.frame.size.height - (self.tblselectService.frame.origin.y/2+self.tblselectService.frame.size.height/2));
        
        self.tblselectService.frame = CGRectMake(self.tblselectService.frame.origin.x, self.tblselectService.frame.origin.y-difference-10, self.tblselectService.frame.size.width, self.tblselectService.frame.size.height);
    }
}

#pragma mark tableview

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrType.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict=[arrType objectAtIndex:indexPath.row];
    SelectServiceCell *cell=(SelectServiceCell *)[tableView dequeueReusableCellWithIdentifier:@"PriceCell"];
    
    cell.lbltitle.text=[dict valueForKey:@"name"];
    cell.txtprice.tag=indexPath.row;
    cell.lblBg.text = NSLocalizedString(@"BASE_PRICE", nil);
    cell.lblBg.layer.cornerRadius = 3;
    cell.lblBg.layer.masksToBounds = YES;
    
    if([[dict valueForKey:@"is_selected"] boolValue])
    {
        cell.txtprice.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"walker_base_price"]];
        cell.imgCheck.image=[UIImage imageNamed:@"service_check"];
        cell.txtprice.background=[UIImage imageNamed:@"bg_price"];
        cell.txtprice.enabled=YES;
        cell.txtdoller.hidden=NO;
        cell.lblBg.hidden=YES;
        NSDictionary *dictType=[arrType objectAtIndex:indexPath.row];
        NSString *strTypeId=[NSString stringWithFormat:@"%@",[dictType valueForKey:@"id"]];
        NSLog(@"selected id : %@",strTypeId);
        
        [arrWtype replaceObjectAtIndex:indexPath.row withObject:strTypeId];
        [arrPricet replaceObjectAtIndex:indexPath.row withObject:cell.txtprice.text];
        
        NSLog(@"price = %@",arrPricet);
        NSLog(@"types = %@",arrWtype);
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0f;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 15)];
    UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(12, 10, 300, 15)];
    lblTitle.font=[UberStyleGuide fontRegular:14.0f];
    lblTitle.textColor=[UIColor darkGrayColor];
    lblTitle.text=NSLocalizedString(@"NON_TOWING_SERVICE", nil);
    [headerView addSubview:lblTitle];
    [headerView setBackgroundColor:[UIColor clearColor]];
    return headerView;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SelectServiceCell *Cell=(SelectServiceCell *)[self.tblselectService cellForRowAtIndexPath:indexPath];
    if([Cell.imgCheck.image isEqual:[UIImage imageNamed:@"unCheckBox"]])
    {
        Cell.imgCheck.image=[UIImage imageNamed:@"check_box"];
        Cell.txtprice.background=[UIImage imageNamed:@"bg_price"];
        Cell.txtprice.enabled=YES;
        [Cell.txtprice becomeFirstResponder];
        Cell.txtdoller.hidden=NO;
        Cell.lblBg.hidden=YES;
        NSDictionary *dictType=[arrType objectAtIndex:indexPath.row];
        NSString *strTypeId=[dictType valueForKey:@"id"];
        Cell.txtdoller.text = [dictType valueForKey:@"currency"];
        NSString *strTypeBasePrice=[dictType valueForKey:@"base_price"];
        Cell.txtprice.text=[NSString stringWithFormat:@"%@",strTypeBasePrice];
        NSLog(@"dict deselect : %@",strTypeId);
        
        [arrWtype replaceObjectAtIndex:indexPath.row withObject:strTypeId];
        //[arrPricet addObject:strTypeBasePrice];
    }
    else
    {
        Cell.imgCheck.image=[UIImage imageNamed:@"unCheckBox"];
        Cell.imgs.image=[UIImage imageNamed:@"bg_price_g"];
        Cell.txtprice.enabled=NO;
        Cell.txtdoller.hidden=YES;
        Cell.lblBg.hidden=NO;
        Cell.txtprice.background=[UIImage imageNamed:@"bg_price_g"];
        NSDictionary *dictType=[arrType objectAtIndex:indexPath.row];
        NSString *strTypeId=[dictType valueForKey:@"id"];
        NSLog(@"dict deselect : %@",strTypeId);
        
        [arrWtype replaceObjectAtIndex:indexPath.row withObject:@""];
        [arrPricet replaceObjectAtIndex:indexPath.row withObject:@""];
        Cell.txtprice.text = @"";
    }
    [self.tblselectService deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)btnCommentClick:(id)sender
{
    UIButton *senderButton = (UIButton *)sender;
    NSIndexPath *path = [NSIndexPath indexPathForRow:senderButton.tag inSection:0];
    SelectServiceCell *cell=[self.tblselectService cellForRowAtIndexPath:path];
    
    NSLog(@"text :%@",cell.lbltitle.text);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action methods

- (IBAction)onClickforRegister:(id)sender
{
    [self.view endEditing:YES];
    
    NSLog(@"price coutn= %lu",(unsigned long)arrPricet.count);
    NSLog(@"types count= %lu",(unsigned long)arrWtype.count);
    
    NSMutableArray *arrFinalType = [[NSMutableArray alloc] init];
    NSMutableArray *arrFinalPrice = [[NSMutableArray alloc] init];
    
    for(int i=0;i<arrWtype.count;i++)
    {
        NSString *str = [NSString stringWithFormat:@"%@",[arrWtype objectAtIndex:i]];
        NSString *str1 = [NSString stringWithFormat:@"%@",[arrPricet objectAtIndex:i]];
        
        if(str.length>0)
        {
            if(str1.length>0)
                [arrFinalPrice addObject:str1];
            else
                [arrFinalPrice addObject:@"0"];
            
            [arrFinalType addObject:str];
        }
    }
    
    NSLog(@"price coutn= %lu",(unsigned long)arrFinalPrice.count);
    NSLog(@"types count= %lu",(unsigned long)arrFinalType.count);
    
    NSString *joinedStringprice11 = [arrFinalPrice componentsJoinedByString:@","];
    NSString *joinedStringprice12 = [arrFinalType componentsJoinedByString:@","];
    
    NSLog(@"price2 :%@",joinedStringprice11);
    NSLog(@"type :%@",joinedStringprice12);
    
    NSMutableDictionary *dictAddDetails = [[NSMutableDictionary alloc] init];
    
    [dictAddDetails setValue:joinedStringprice12 forKey:PARAM_WALKER_TYPE];
    [dictAddDetails setValue:joinedStringprice11 forKey:PARAM_WALKER_PRICE];
    
    [self.dictparam addEntriesFromDictionary:dictAddDetails];
    
    [self performSegueWithIdentifier:@"segueToTwingService" sender:arrForTowingServiceType];
}

- (IBAction)onClickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark prepare for segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"segueToTwingService"])
    {
        SelectTowingServiceVC *sel=[segue destinationViewController];
        sel.arr = [[NSMutableArray alloc] init];
        [sel.arr addObjectsFromArray:arrForTowingServiceType];
        sel.dictParam = [[NSMutableDictionary alloc] init];
        [sel.dictParam addEntriesFromDictionary:self.dictparam];
        sel.imgPicture=self.imgP;
        sel.arrForTimeCost = [[NSMutableArray alloc] init];
        sel.arrForDistanceCost = [[NSMutableArray alloc] init];
        [sel.arrForDistanceCost addObjectsFromArray:arrDistancePrice];
        [sel.arrForTimeCost addObjectsFromArray:arrTimePrice];
        
        NSLog(@"arr = %@",sel.arr);
        NSLog(@"dict = %@",sel.dictParam);
    }
}

@end