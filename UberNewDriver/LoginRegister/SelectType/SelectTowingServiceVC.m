//
//  SelectTowingServiceVC.m
//  Moto Help UK Provider
//
//  Created by Elluminati Macbook Pro 2 on 9/17/16.
//  Copyright © 2016 Deep Gami. All rights reserved.
//

#import "SelectTowingServiceVC.h"
#import "TowingTypeCell.h"

@interface SelectTowingServiceVC ()
{
    NSMutableArray *arrType;
    NSMutableArray *arrForPriceCost;
    NSMutableArray *arrDistanceCost;
    NSMutableArray *arrTimeCost;
    NSMutableArray *arrBaseDistance;
    CGRect serviceTableFrame;
}

@end

@implementation SelectTowingServiceVC
@synthesize arr;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    arrType = [[NSMutableArray alloc]init];
    arrForPriceCost = [[NSMutableArray alloc] init];
    arrDistanceCost = [[NSMutableArray alloc] init];
    arrTimeCost = [[NSMutableArray alloc] init];
    arrBaseDistance = [[NSMutableArray alloc] init];
    
    for(int i=0;i<arr.count;i++)
    {
        [arrType addObject:@""];
        [arrForPriceCost addObject:@""];
        [arrDistanceCost addObject:@""];
        [arrTimeCost addObject:@""];
        [arrBaseDistance addObject:@""];
    }
    
    [self setLocalization];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.tableForTowingTypes.contentInset = UIEdgeInsetsZero;
    self.automaticallyAdjustsScrollViewInsets = NO;
    serviceTableFrame = self.tableForTowingTypes.frame;
}

-(void)viewWillDisappear:(BOOL)animated
{
    //self.navigationController.navigationBarHidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - custom methods

-(void)setLocalization
{
    [self.btnMenu setTitle:NSLocalizedString(@"SELECR_SERVICE", nil) forState:UIControlStateNormal];
    [self.btnMenu setTitle:NSLocalizedString(@"SELECR_SERVICE", nil) forState:UIControlStateHighlighted];
    [self.btnRegister setTitle:NSLocalizedString(@"REGISTER", nil) forState:UIControlStateNormal];
    [self.btnRegister setTitle:NSLocalizedString(@"REGISTER", nil) forState:UIControlStateHighlighted];
    self.btnMenu.titleLabel.font = [UberStyleGuide fontRegularLight];
}

#pragma mark tableview

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arr.count;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 15)];
    UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(12, 10, 300, 15)];
    lblTitle.font=[UberStyleGuide fontRegular:14.0f];
    lblTitle.textColor=[UIColor darkGrayColor];
    lblTitle.text=NSLocalizedString(@"TOWING_SERVICE", nil);
    [headerView addSubview:lblTitle];
    [headerView setBackgroundColor:[UIColor clearColor]];
    return headerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict=[arr objectAtIndex:indexPath.row];
    
    TowingTypeCell *cell=(TowingTypeCell *)[tableView dequeueReusableCellWithIdentifier:@"PriceCell"];
    
    cell.lbltitle.text=[dict valueForKey:@"name"];
    
    cell.txtprice.tag=indexPath.row;
    cell.lblBg.text = NSLocalizedString(@"DISTANCE_PRICE", nil);
    cell.lblBg.layer.cornerRadius = 3;
    cell.lblBg.layer.masksToBounds = YES;
    cell.lblBgBase.text = NSLocalizedString(@"BASE_PRICE", nil);
    cell.lblBgBase.layer.cornerRadius = 3;
    cell.lblBgBase.layer.masksToBounds = YES;
    cell.lblBgTime.text = NSLocalizedString(@"TIME_PRICE", nil);
    cell.lblBgTime.layer.cornerRadius = 3;
    cell.lblBgTime.layer.masksToBounds = YES;
    cell.lblBgBaseDistance.text = NSLocalizedString(@"BASE_DISTANCE", nil);
    cell.lblBgBaseDistance.layer.cornerRadius = 3;
    cell.lblBgBaseDistance.layer.masksToBounds = YES;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TowingTypeCell *Cell=(TowingTypeCell *)[self.tableForTowingTypes cellForRowAtIndexPath:indexPath];
    
    if([Cell.imgCheck.image isEqual:[UIImage imageNamed:@"unCheckBox"]])
    {
        Cell.imgCheck.image=[UIImage imageNamed:@"check_box"];
        Cell.txtprice.background=[UIImage imageNamed:@"bg_price"];
        Cell.txtprice.enabled=YES;
        Cell.lblCurrency.hidden=NO;
        Cell.lblCurrencyTime.hidden=NO;
        Cell.lblCurrencyBase.hidden=NO;
        Cell.lblCurrencyBaseDistance.hidden=NO;
        Cell.lblBg.hidden=YES;
        Cell.lblBgTime.hidden=YES;
        Cell.lblBgBase.hidden=YES;
        Cell.lblBgBaseDistance.hidden=YES;
        NSDictionary *dictType=[arr objectAtIndex:indexPath.row];
        NSString *strTypeId=[dictType valueForKey:@"id"];
        Cell.lblCurrency.text = [dictType valueForKey:@"currency"];
        Cell.lblCurrencyTime.text = [dictType valueForKey:@"currency"];
        Cell.lblCurrencyBase.text = [dictType valueForKey:@"currency"];
        Cell.lblCurrencyBaseDistance.text = [dictType valueForKey:@"currency"];
        NSString *strTypeBasePrice=[dictType valueForKey:@"base_price"];
        NSString *strTypeDistancePrice=[dictType valueForKey:@"price_per_unit_distance"];
        NSString *strTypeTimePrice=[dictType valueForKey:@"price_per_unit_time"];
        NSString *strTypeBaseDistance=[dictType valueForKey:@"base_distance"];
        Cell.txtprice.text=[NSString stringWithFormat:@"%@",strTypeBasePrice];
        Cell.txtTimePrice.text=[NSString stringWithFormat:@"%@",strTypeTimePrice];
        Cell.txtDistancePrice.text=[NSString stringWithFormat:@"%@",strTypeDistancePrice];
        Cell.txtBaseDistance.text=[NSString stringWithFormat:@"%@",strTypeBaseDistance];
        NSLog(@"dict deselect : %@",strTypeId);
        
        [arrType replaceObjectAtIndex:indexPath.row withObject:strTypeId];
        [arrDistanceCost replaceObjectAtIndex:indexPath.row withObject:strTypeDistancePrice];
        [arrTimeCost replaceObjectAtIndex:indexPath.row withObject:strTypeTimePrice];
        [arrForPriceCost replaceObjectAtIndex:indexPath.row withObject:strTypeBasePrice];
        [arrBaseDistance replaceObjectAtIndex:indexPath.row withObject:strTypeBaseDistance];
    }
    else
    {
        Cell.imgCheck.image=[UIImage imageNamed:@"unCheckBox"];
        Cell.imgs.image=[UIImage imageNamed:@"bg_price_g"];
        Cell.imgTime.image=[UIImage imageNamed:@"bg_price_g"];
        Cell.imgBase.image=[UIImage imageNamed:@"bg_price_g"];
        Cell.txtprice.enabled=NO;
        Cell.lblBg.hidden=NO;
        Cell.lblBgTime.hidden=NO;
        Cell.lblBgBase.hidden=NO;
        Cell.lblBgBaseDistance.hidden=NO;
        Cell.lblCurrency.hidden=YES;
        Cell.lblCurrencyTime.hidden=YES;
        Cell.lblCurrencyBase.hidden=YES;
        Cell.lblCurrencyBaseDistance.hidden=YES;
        Cell.txtprice.background=[UIImage imageNamed:@"bg_price_g"];
        NSDictionary *dictType=[arr objectAtIndex:indexPath.row];
        NSString *strTypeId=[dictType valueForKey:@"id"];
        NSLog(@"dict deselect : %@",strTypeId);
        
        [arrType replaceObjectAtIndex:indexPath.row withObject:@""];
        [arrTimeCost replaceObjectAtIndex:indexPath.row withObject:@""];
        [arrDistanceCost replaceObjectAtIndex:indexPath.row withObject:@""];
        [arrForPriceCost replaceObjectAtIndex:indexPath.row withObject:@""];
        [arrBaseDistance replaceObjectAtIndex:indexPath.row withObject:@""];
        
        Cell.txtprice.text = @"";
        Cell.txtDistancePrice.text=@"";
        Cell.txtTimePrice.text=@"";
        Cell.txtBaseDistance.text=@"";
    }
    
    [self.tableForTowingTypes deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - textfield delagte method

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
    
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    return [string isEqualToString:filtered];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    UITextField *txtf=(UITextField *)textField;
    NSIndexPath *path = [NSIndexPath indexPathForRow:txtf.tag inSection:0];
    
    [textField resignFirstResponder];
    
    TowingTypeCell *cell=[self.tableForTowingTypes cellForRowAtIndexPath:path];
    
    if(textField == cell.txtprice)
    {
        if(cell.txtprice.text.length>0)
            [arrForPriceCost replaceObjectAtIndex:path.row withObject:cell.txtprice.text];
    }
    else if (textField == cell.txtDistancePrice)
    {
        if(cell.txtDistancePrice.text.length>0)
            [arrDistanceCost replaceObjectAtIndex:path.row withObject:cell.txtDistancePrice.text];
    }
    else if (textField == cell.txtTimePrice)
    {
        if(cell.txtTimePrice.text.length>0)
            [arrTimeCost replaceObjectAtIndex:path.row withObject:cell.txtTimePrice.text];
    }
    else if (textField == cell.txtBaseDistance)
    {
        if(cell.txtBaseDistance.text.length>0)
            [arrBaseDistance replaceObjectAtIndex:path.row withObject:cell.txtBaseDistance.text];
    }
    
    NSLog(@"index  :%ld",(long)txtf.tag);
    
    NSLog(@"log price:%@",arrForPriceCost);
    NSLog(@"log distance:%@",arrDistanceCost);
    NSLog(@"log time:%@",arrTimeCost);
    NSLog(@"log Base distance:%@",arrBaseDistance);
    
    self.tableForTowingTypes.frame = serviceTableFrame;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    self.tableForTowingTypes.frame = serviceTableFrame;
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint origin = textField.frame.origin;
    CGPoint point = [textField.superview convertPoint:origin toView:self.tableForTowingTypes];
    
    NSIndexPath *indexPath = [self.tableForTowingTypes indexPathForRowAtPoint:point];
    
    TowingTypeCell *cell=[self.tableForTowingTypes cellForRowAtIndexPath:indexPath];
    
    NSLog(@"indexPath = %ld",(long)indexPath.row);
    
    NSLog(@"Cell Frame = %f",cell.frame.origin.y+cell.frame.size.height);
    
    self.tableForTowingTypes.frame = serviceTableFrame;
    
    NSLog(@"View Frame Center = %f",self.tableForTowingTypes.frame.origin.y/2+self.tableForTowingTypes.frame.size.height/2);
    
    NSInteger difference = (cell.frame.origin.y+cell.frame.size.height - (self.tableForTowingTypes.frame.origin.y/2+self.tableForTowingTypes.frame.size.height/2));
    
    NSLog(@"Difference = %ld",(long)difference);
    
    if(self.tableForTowingTypes.frame.origin.y/2+self.tableForTowingTypes.frame.size.height/2 < cell.frame.origin.y+cell.frame.size.height)
    {
        NSInteger difference = (cell.frame.origin.y+cell.frame.size.height - (self.tableForTowingTypes.frame.origin.y/2+self.tableForTowingTypes.frame.size.height/2));
        
        self.tableForTowingTypes.frame = CGRectMake(self.tableForTowingTypes.frame.origin.x, self.tableForTowingTypes.frame.origin.y-difference-10, self.tableForTowingTypes.frame.size.width, self.tableForTowingTypes.frame.size.height);
    }
}

#pragma mark - Action methods

- (IBAction)OnClickRegister:(id)sender
{
    [self.view endEditing:YES];
    
    NSString *str;
    NSString *strTpe = [self.dictParam valueForKey:PARAM_WALKER_TYPE];
    NSString *strPrice = [self.dictParam valueForKey:PARAM_WALKER_PRICE];
    
    NSMutableArray *arrTowingType = [[NSMutableArray alloc] init];
    
    for (int i=0; i<arrType.count; i++)
    {
        str = [arrType objectAtIndex:i];
        
        if(str.length>0)
        {
            [arrTowingType addObject:str];
        }
    }
    
    if(arrTowingType.count>0 || strTpe.length>0)
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
        
        NSMutableArray *arrFinalDistance = [[NSMutableArray alloc] init];
        NSMutableArray *arrFinalTime = [[NSMutableArray alloc] init];
        NSMutableArray *arrFinalPrice = [[NSMutableArray alloc] init];
        NSMutableArray *arrFinalType = [[NSMutableArray alloc] init];
        NSMutableArray *arrFinalBaseDistance = [[NSMutableArray alloc] init];
        
        for(int i=0;i<arrType.count;i++)
        {
            NSString *str = [NSString stringWithFormat:@"%@",[arrType objectAtIndex:i]];
            NSString *strDistance = [NSString stringWithFormat:@"%@",[arrDistanceCost objectAtIndex:i]];
            NSString *strTime = [NSString stringWithFormat:@"%@",[arrTimeCost objectAtIndex:i]];
            NSString *strPrice = [NSString stringWithFormat:@"%@",[arrForPriceCost objectAtIndex:i]];
            NSString *strBaseDistace = [NSString stringWithFormat:@"%@",[arrBaseDistance objectAtIndex:i]];
            
            if(str.length>0)
            {
                if(strDistance.length>0)
                    [arrFinalDistance addObject:strDistance];
                else
                    [arrFinalDistance addObject:@"0"];
                
                if(strTime.length>0)
                    [arrFinalTime addObject:strTime];
                else
                    [arrFinalTime addObject:@"0"];
                
                if(strPrice.length>0)
                    [arrFinalPrice addObject:strPrice];
                else
                    [arrFinalPrice addObject:@"0"];
                
                if(strBaseDistace.length>0)
                    [arrFinalBaseDistance addObject:strBaseDistace];
                else
                    [arrFinalBaseDistance addObject:@"0"];
                
                [arrFinalType addObject:str];
            }
        }
        
        NSString *joinedStringprice1 = [arrFinalDistance componentsJoinedByString:@","];
        NSString *joinedStringprice2 = [arrFinalTime componentsJoinedByString:@","];
        NSString *joinedStringprice3 = [arrFinalPrice componentsJoinedByString:@","];
        NSString *joinedStringprice4 = [arrFinalType componentsJoinedByString:@","];
        NSString *joinedStringprice5 = [arrFinalBaseDistance componentsJoinedByString:@","];
        
        [self.dictParam removeObjectForKey:PARAM_WALKER_TYPE];
        [self.dictParam removeObjectForKey:PARAM_WALKER_PRICE];
        
        // add distance and time parameter with 0 in perticuler array
        
        NSMutableArray *arrDistanceTime = [[NSMutableArray alloc] init];
        NSMutableDictionary *dictAddDetails;
        NSString *strDistanceOfFirstPageType;
        NSString *strTimeOfFirstPageType;
        NSString *strBaseDistanceOfFirstPageType;
        
        if(strTpe.length>0)
        {
            NSArray *arrDistance = [strTpe componentsSeparatedByString:@","];
            
            for(int k=0;k<arrDistance.count;k++)
            {
                [arrDistanceTime addObject:@"0"];
            }
            
            strDistanceOfFirstPageType = [arrDistanceTime componentsJoinedByString:@","];
            
            strTimeOfFirstPageType = [arrDistanceTime componentsJoinedByString:@","];
            
            strBaseDistanceOfFirstPageType = [arrDistanceTime componentsJoinedByString:@","];
            
            
            strDistanceOfFirstPageType = [strDistanceOfFirstPageType stringByAppendingString:[NSString stringWithFormat:@",%@",joinedStringprice1]];
            
            strTimeOfFirstPageType = [strTimeOfFirstPageType stringByAppendingString:[NSString stringWithFormat:@",%@",joinedStringprice2]];
            
            strTpe = [strTpe stringByAppendingString:[NSString stringWithFormat:@",%@",joinedStringprice4]];
            
            strPrice = [strPrice stringByAppendingString:[NSString stringWithFormat:@",%@",joinedStringprice3]];
            
            strBaseDistanceOfFirstPageType = [strBaseDistanceOfFirstPageType stringByAppendingString:[NSString stringWithFormat:@",%@",joinedStringprice5]];
            
            dictAddDetails = [[NSMutableDictionary alloc] init];
            
            [dictAddDetails setValue:strDistanceOfFirstPageType forKey:PARAM_WALKER_DISTANCE_PRICE];
            [dictAddDetails setValue:strTimeOfFirstPageType forKey:PARAM_WALKER_TIME_PRICE];
            [dictAddDetails setValue:strTpe forKey:PARAM_WALKER_TYPE];
            [dictAddDetails setValue:strPrice forKey:PARAM_WALKER_PRICE];
            [dictAddDetails setValue:strBaseDistanceOfFirstPageType forKey:PARAM_WALKER_BASE_DISTANCE];
            
        }
        else
        {
            dictAddDetails = [[NSMutableDictionary alloc] init];
            
            [dictAddDetails setValue:joinedStringprice1 forKey:PARAM_WALKER_DISTANCE_PRICE];
            [dictAddDetails setValue:joinedStringprice2 forKey:PARAM_WALKER_TIME_PRICE];
            [dictAddDetails setValue:joinedStringprice4 forKey:PARAM_WALKER_TYPE];
            [dictAddDetails setValue:joinedStringprice3 forKey:PARAM_WALKER_PRICE];
            [dictAddDetails setValue:joinedStringprice5 forKey:PARAM_WALKER_BASE_DISTANCE];
        }
        
        [self.dictParam addEntriesFromDictionary:dictAddDetails];
        
        NSLog(@"Dictionary = %@",self.dictParam);
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        
        [afn getDataFromPath:FILE_REGISTER withParamDataImage:self.dictParam andImage:self.imgPicture withBlock:^(id response, NSError *error)
         {
             [APPDELEGATE hideLoadingView];
             if (response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     [APPDELEGATE showToastMessage:(NSLocalizedString(@"REGISTER_SUCCESS", nil))];
                     arrUser=response;
                     NSLog(@"res :%@",response);
                     [self.navigationController popToRootViewControllerAnimated:YES];
                 }
                 else
                 {
                     NSString *str = [NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                     
                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(str, nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                     [alert show];
                 }
             }
             
             NSLog(@"REGISTER RESPONSE --> %@",response);
         }];
    }
    else
    {
        [APPDELEGATE showToastMessage:NSLocalizedString(@"PLEASE_SELECT_SERVICE", nil)];
    }
}

- (IBAction)onClickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
