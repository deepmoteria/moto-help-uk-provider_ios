//
//  TowingTypeCell.h
//  Moto Help UK Provider
//
//  Created by Elluminati Macbook Pro 2 on 9/17/16.
//  Copyright © 2016 Deep Gami. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TowingTypeCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbltitle;


@property (weak, nonatomic) IBOutlet UILabel *lblBgBaseDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblBgTime;
@property (weak, nonatomic) IBOutlet UILabel *lblBgBase;
@property (weak, nonatomic) IBOutlet UILabel *lblBg;


@property (weak, nonatomic) IBOutlet UIImageView *imgs;
@property (weak, nonatomic) IBOutlet UIImageView *imgTime;
@property (weak, nonatomic) IBOutlet UIImageView *imgBase;
@property (weak, nonatomic) IBOutlet UIImageView *imgBaseDistance;

@property (weak, nonatomic) IBOutlet UITextField *txtDistancePrice;
@property (weak, nonatomic) IBOutlet UITextField *txtprice;
@property (weak, nonatomic) IBOutlet UITextField *txtTimePrice;
@property (weak, nonatomic) IBOutlet UITextField *txtBaseDistance;


@property (weak, nonatomic) IBOutlet UILabel *lblCurrency;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrencyTime;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrencyBase;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrencyBaseDistance;


@property (weak, nonatomic) IBOutlet UIImageView *imgCheck;
@end
