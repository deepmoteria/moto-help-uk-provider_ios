//
//  SelectTowingServiceVC.h
//  Moto Help UK Provider
//
//  Created by Elluminati Macbook Pro 2 on 9/17/16.
//  Copyright © 2016 Deep Gami. All rights reserved.
//

#import "BaseVC.h"

@interface SelectTowingServiceVC : BaseVC<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (strong, nonatomic)IBOutlet NSMutableArray *arr;
@property (strong, nonatomic)IBOutlet NSMutableArray *arrForDistanceCost;
@property (strong, nonatomic)IBOutlet NSMutableArray *arrForTimeCost;
@property (strong, nonatomic)IBOutlet NSMutableDictionary *dictParam;
@property (strong,nonatomic) UIImage *imgPicture;
@property (weak, nonatomic) IBOutlet UITableView *tableForTowingTypes;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;

- (IBAction)OnClickRegister:(id)sender;
- (IBAction)onClickBack:(id)sender;



@end
